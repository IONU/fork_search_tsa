
var app = angular.module('pacient_form');

app.controller('QuestionsAddCtrl', ['$scope','$http', function ($scope, $http) {
    $scope.list = [];
    $scope.intrebare = '';
    $scope.formData = {};
    $scope.submit_question = function() {
        $http({
            method  : 'POST',
            url     : 'do_add_questions',
            data    : $.param($scope.formData),  // pass in data as strings
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
        })
            .success(function(data) {
                console.log(data);


                // if successful, bind success message to message
                $http.get('/questions')
                    .success(function (data) {
                        $scope.groups = data;
                    });


            });
    };
    $scope.getList = function (){
        $http.get('/questions')
            .success(function (data) {
                $scope.groups = data;
            });
    };
    $scope.getQuestionsList = function (){
        $http.get('/questions_list')
            .success(function (data) {
                $scope.groups = data;
            });
    };

    $scope.addFields = function () {
        if(typeof $scope.formData.answers === 'undefined') {
            $scope.formData.answers = [];
        }
        $scope.formData.answers.push({raspuns:'', punctaj_raspuns: '' });
    }

    $scope.oneAtATime = false;

    $scope.status = {
        isFirstOpen: true,
        isFirstDisabled: false,
        isOpen:true
    }
}]);
