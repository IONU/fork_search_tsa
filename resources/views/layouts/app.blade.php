<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Search TSA</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css">
    <link href="/css/app.css" rel="stylesheet">
    <!-- Fonts -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <link href="/js/angular-xeditable/css/xeditable.css" rel="stylesheet">
    <script src="/js/angular-xeditable/js/xeditable.js"></script>
    <![endif]-->
    {{--<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.3/angular.min.js"></script>--}}
    <style>
        .navbar-custom {
            color: #FFFFFF;
            background-color: #0b4272;
        }

        html,
        body {
            margin: 0;
            padding: 0;
            height: 100%;
        }

        #wrapper {
            min-height: 100%;
            position: relative;
        }

        #header {
            background: #ededed;
            padding: 10px;
        }

        #content {
            padding-bottom: 30px; /* Height of the footer element */
        }

        footer {
           padding-bottom: 30px; 
        }
    </style>
</head>
<body>
<div id="wrapper">
    <div id='content'>
        <div class="masthead">
            <table width="90%" style="margin-left: 100px; margin-right: 110px; align-content: center;">
                <tr>
                    <td align="center">
                        <a href="http://autismbaiamare.ro" title="Asociatia Autism Baia Mare"><img
                                    style="max-width:150px; margin-bottom: 10px; "
                                    src="/pictures/logo_AutismBaiaMare.gif"></a>
                    </td>
                    <td align="center">
                        <a href="http://www.autismfedra.ro/"
                           title="Federatia pentru Drepturi si Resurse pentru Persoanele cu Tulburari in Spectrul Autist"><img
                                    style="max-width:200px; " src="/pictures/logo_fedra.png"></a>
                    </td>
                    <td align="center">
                        <a href="http://www.snmf.ro/" title="Societatea Nationala de Medicina Familiei"><img
                                    style="max-width:200px; " src="/pictures/logo_snmf.jpg"></a>
                    </td>
                    <td align="center">
                        <a href="https://www.vodafone.ro/despre-noi/implicare-sociala/fundatia-vodafone/"
                           title="Fundatia Vodafone Romania"><img style="max-width:200px; "
                                                                  src="/pictures/logo_f_vdf_rosu.jpg"></a>
                    </td>
                </tr>
            </table>
        </div>
        <nav class="navbar navbar-inverse navbar-custom" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"></a>
                </div>

                <div class="collapse navbar-collapse " id="bs-example-navbar-collapse-1">
                    @if (!Auth::guest())
                        <ul class="nav navbar-nav">
                            <li><a href="/">Home</a></li>
                            <li><a href="/pacient/adauga">Adauga pacient nou</a></li>
                            <li><a href="/pacient/list">Lista pacienti</a></li>
                            @if ( Auth::user()->is_admin == 1)
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Admin <b
                                                class="caret"></b></a>

                                    <ul class="dropdown-menu">
                                        <li class="kopie"><a href="/admin/lista_pacienti">Lista pacienti</a></li>
                                        <li class="kopie"><a href="/admin/lista_utilizatori">Lista utilizatori</a></li>
                                        <li><a href="#">Dropdown Link 3</a></li>

                                        <li class="dropdown dropdown-submenu"><a href="#" class="dropdown-toggle"
                                                                                 data-toggle="dropdown">Dropdown Link
                                                4</a>
                                            <ul class="dropdown-menu">
                                                <li class="kopie"><a href="#">Dropdown Link 4</a></li>
                                                <li><a href="#">Dropdown Submenu Link 4.1</a></li>
                                                <li><a href="#">Dropdown Submenu Link 4.2</a></li>
                                                <li><a href="#">Dropdown Submenu Link 4.3</a></li>
                                                <li><a href="#">Dropdown Submenu Link 4.4</a></li>

                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            @endif
                        </ul>
                    @endif
                    <ul class="nav navbar-nav navbar-right">
                        @if (Auth::guest())
                            <li><a href="/auth/login">Login | medici de familie</a></li>
                            <li><a href="/auth/register">Creaza cont | medici de familie</a></li>
                            <li><a href="/vizualizeaza_chestionar" title="poti sa testezi daca copilul tau are sau nu simptomele unei tulburari din spectrul autist in mod anonim, fara sa divulgi identitatea ta sau a copilului tau">Aplica chestionarul anonim | parinte</a></li>
                        @else
                            <li><a href="/user/detalii">Contul meu</a></li>
                            <li><a href="/auth/logout">Logout</a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Scripts -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
        <script data-require="angular.js@1.4.3" data-semver="1.4.3"
                src="http://code.angularjs.org/1.4.3/angular.js"></script>
        @yield('content')
        
        
        
    </div>
    <footer>
       <div style="text-align:center; margin-bottom: 20px;">
       	&copy2015 Asociatia Autism Baia Mare |
       	Proiect finantat de <a href="https://www.vodafone.ro/despre-noi/implicare-sociala/fundatia-vodafone/index.htm?cid=rdr-ro-fundatia-vodafone" class="redlink">Fundatia Vodafone Romania</a> prin
            programul "<a
                    href="https://www.vodafone.ro/despre-noi/implicare-sociala/fundatia-vodafone/proiecte/mobile-for-good/proiecte/" class="redlink">Mobile
                for Good</a>"
        </div>
    </footer>
</div>
</body>
</html>
