@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">SEARCH TSA</div>
				<div class="panel-body">
					<ul>Proiectul SEARCH TSA  vizeaza trei arii importante:<br/><br/>

						<li>crearea unei baze de date centralizate in screening-ul initial al copiilor si a unei platforme cu teste de evaluare standardizate
							pentru evaluarea copiilor cu autism de catre psihologi;</li>

						<li>infiintarea  unui HELPLINE AUTISM pentru a veni in sprijinul familiilor care se confrunta cu aceasta problema dar si a
							celorlate categorii de actori sociali care vin in contact cu copiii;</li>

						<li>crearea de  50 de aplicatii  care ofera o  interfata foarte utila si motivanta in interventiile terapeutice si educationale
							destinate copiilor cu autism. Tehnologia  poate fi folosita pentru a imbunatati comunicarea verbala dar in acelasi timp ofera o a
							lternativa de comunicare prin aplicatii pentru copiii non-verbali; ajuta la dezvoltarea aptitudinilor sociale prin jocurile specifice
							disponibile; sporesc capacitatea de a invata prin stimulii vizuali, auditivi  si tactili. Aplicatiile existente in acest moment pe
							piata sunt fie in limba engleza, fie au minusuri cu privire la continut acestea fiind create pentru copii tipici, nu pentru copii cu
							autism.</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
