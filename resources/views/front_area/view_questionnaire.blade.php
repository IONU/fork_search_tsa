@extends('layouts.app') @section('content')
    <script data-require="angular-messages@*" data-semver="1.4.3"
            src="https://code.angularjs.org/1.4.3/angular-messages.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.3/angular.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.3/angular-animate.js"></script>
    <script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.13.3.js"></script>
    <script src="/js/user_area/create_pacient.js"></script>
    <script src="/js/admin_area/QuestionsAddCtrl.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.8/jquery.form-validator.min.js"></script>
    <div class="container">
        <div id='questionnaire' ng-app="pacient_form">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">La intrebarile din chestionar pot sa raspunda anonim <strong>parintii sau educatorii</strong>. <br/>
                    	<strong>Medicii de familie</strong> sunt rugati sa se <a href="/auth/login">autentifice</a> pentru ca informatiile introduse sa fie salvate in baza de date si procesate statistic ulterior.</div>

                    <div class="panel-group panel-body" id="accordion" ng-init="getList()" ng-controller="QuestionsAddCtrl">

                    <div class="alert alert-info" role="alert">
                        Acest test este primul pas in identificarea unei suspiciuni de autism.
                        Poti sa il completezi anonim sa vezi daca este ceva anormal cu copilul tau.
                        Indiferent de rezultat e bine sa stii ca AUTISMUL POATE FI INVINS!
                    </div>
                    <div class="alert alert-info" role="alert">
                        <p>Testul este adaptat dupa "Modified Checklist for Autism in Toddlers" (M-CHAT) (© 1999 Diana
                        Robins, Deborah Fein & Marianne Barton).
                        Are rolul de a depista autismul la copii intre 16 si 30 luni, insa nu ofera un diagnostic
                        cert de autism la copil, ci doar o supozitie de diagnostic (probabilitate) de afectiune de tip autist.
                      	</p>
                        <p>
                      <strong>Daca testul este pozitiv acest lucru nu garanteaza ca acel copil are autism. Este recomandata evaluare de catre psiholog/psihiatru pediatru</strong>.
                        </p>
                       	<p>
                        Daca testul este negativ - copilul nu are autism.
                        </p> 
                    </div>

										<div class="center-txt alert alert-info " role="alert">
											Pentru mai multe informatii, detalii, sfaturi sau consiliere de specialitate despre autism, apelati <br/><strong>infoline Autism: 0800 500288</strong>
										</div>

                    <br/>
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li> @endforeach
                                </ul>
                            </div>
                        @endif


                        <form method="post" action="/proceseaza_chestionar">
                        <div>
                            <div class="form row" ng-repeat="group in groups track by $index">
                                <label class="control-label" style="margin-left:10px;">
                                    @{{group.intrebare}}  <br/> </label>

                                <div class="">
                                    <span ng-repeat="answer in group.answers"
                                          style="width: 400px; float:left; margin-left:10px; margin-bottom: 10px;">
                                    <label class="radio-inline">
                                        <input name="intrebari[@{{group.id}}]" id="@{{answer.id}}" data-validation="required" data-validation-error-msg="Raspunsul la aceasta intrebare este obligatoriu"
                                               ng-value="answer.id" type="radio"  ng-model="intrebari"> @{{answer.raspuns }}
                                        <span style="color: red" ng-show="(create_form.intrebari.$dirty || submitted) && create_form.intrebari.$invalid">
                                                <span ng-show="create_form.intrebari.$error.required">Câmpul Email este obligatoriu.</span>
                                            </span>
                                    </label>
                                        </span>
                                </div>
                            </div>
                        </div>


                    <input class="btn btn-primary btn-block"  type="submit" ng-click="submit()" value="Trimite" >


                </form>
                </div>
                </div>
            </div>
        </div>
    </div>

                            <style>
                                .row:nth-of-type(even) {
                                    background: #D9EDF7;
                                }
                            </style>
    <script> $.validate(); </script>

@stop
