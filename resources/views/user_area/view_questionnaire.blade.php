@extends('layouts.app') @section('content')
    <script data-require="angular-messages@*" data-semver="1.4.3"
            src="https://code.angularjs.org/1.4.3/angular-messages.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.3/angular.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.3/angular-animate.js"></script>
    <script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.13.3.js"></script>
    <script src="/js/user_area/create_pacient.js"></script>
    <script src="/js/admin_area/QuestionsAddCtrl.js"></script>
    <div class="container">
        <div id='questionnaire' ng-app="pacient_form">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Intrebari la care pot sa raspunda parintii si/sau educatorii</div>
                    <br/>

                    <div class="alert alert-info alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert"><span
                                    aria-hidden="true">&times;</span><span
                                    class="sr-only">Close</span></button>
                        <button type="button" class="close" data-dismiss="alert"><span
                                    aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        Acest test este adaptat dupa "Modified Checklist for Autism in Toddlers" (M-CHAT) (© 1999 Diana
                        Robins, Deborah Fein & Marianne Barton).
                        Testul are rolul de a depista autismul la copii intre 16 si 30 luni. El nu ofera un diagnostic
                        cert
                        de autism la copil, ci doar o supozitie de diagnostic (probabilitate) de afectiune de tip
                        autist.
                        <br/>
                        Daca testul este pozitiv pentru autism:
                        - acest lucru nu garanteaza ca acel copil are autism
                        - este recomandata evaluare de catre psiholog/psihiatru pediatru
                        <br/>
                        Daca testul este negativ - copilul nu are autism.
                    </div>

                    <br/>

                    <div class="panel-group panel-body" id="accordion" ng-init="getQuestionsList()" ng-controller="QuestionsAddCtrl">

                        <div>
                            <div class="form row" ng-repeat="group in groups track by $index">
                                <label class="control-label" style="margin-left:10px;">
                                    @{{group.intrebare}}  <br/> </label>

                                <div class="">
                                    <span ng-repeat="answer in group.answers"
                                          style="width: 400px; float:left; margin-left:10px; margin-bottom: 10px;">
                                    <label class="radio-inline">
                                        @{{answer.raspuns }}
                                    </label>
                                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <style>
        .row:nth-of-type(even) {
            background: #D9EDF7;
        }
    </style>

@stop
