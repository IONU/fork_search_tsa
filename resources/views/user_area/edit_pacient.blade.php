@extends('layouts.app') @section('content')
<script data-require="angular-messages@*" data-semver="1.4.3" src="https://code.angularjs.org/1.4.3/angular-messages.min.js"></script>
<script type="text/javascript" src="/js/ngRemoteValidate.0.6.1.min.js"></script>
<script src="/js/user_area/create_pacient.js"></script> 
 

<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Editeaza Pacient</div>
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li> @endforeach
					</ul>
				</div>
				@endif
				<div class="panel-body">
					<form id="create_form" action="/pacient/do_edit/{{ $patient->id }}" method="post"
						ng-app="pacient_form" ng-controller="validateCtrl" name="create_form"
						novalidate>
						
						<div class="form-group">
							<label class="col-md-4 control-label">Nume</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="nume" id="nume"
									ng-init="nume='{{ $patient->nume }}'" ng-model="nume" required ng-pattern="/^[a-zA-Z\s]*$/"> 
								<span style="color: red" ng-show="(create_form.nume.$dirty || submitted) && create_form.nume.$invalid"> 
									<span ng-show="create_form.nume.$error.required">Câmpul este obligatoriu.</span>
									<span ng-show="create_form.nume.$error.pattern">Câmpul poate contine doar litere si spatiu.</span>
								</span>
							</div>
							<label class="col-md-4 control-label">Prenume</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="prenume"
									ng-init="prenume='{{ $patient->prenume }}'" ng-model="prenume" required ng-pattern="/^[a-zA-Z\s]*$/"> 
								<span style="color: red" ng-show="(create_form.prenume.$dirty || submitted) && create_form.prenume.$invalid"> 
									<span ng-show="create_form.prenume.$error.required">Câmpul este obligatoriu.</span>
									<span ng-show="create_form.prenume.$error.pattern">Câmpul poate contine doar litere si spatiu.</span>
								</span>
							</div>
							<label class="col-md-4 control-label">CNP</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="cnp"
									ng-init="cnp='{{ $patient->cnp }}'" ng-model="cnp" required ng-pattern="/^[0-9]*$/" ng-minlength="13" ng-maxlength="13"
									 ng-remote-method="POST"
									 ng-remote-validate='/pacient/verfy_cnp/' 
								> 
								<span style="color: red" ng-show="(create_form.cnp.$dirty || submitted) && create_form.cnp.$invalid"> 
									<div ng-messages="create_form.cnp.$error">
                                    <span ng-show="create_form.prenume.$error.required">Câmpul este obligatoriu.</span>
										<div ng-messages-include="/error_messages/default_error_messages"></div>
									    <span ng-message="pattern">Câmpul CNP poate contine doar numere.</span>
                                            <span ng-message="minlength, maxlength">Câmpul CNP trebuie sa aiba 13 caractere.</span>
									</div>
								</span>
								
								<span style="color: red" ng-show="create_form.cnp.$error.ngRemoteValidate">
							        CNP existent in baza de date
							    </span>
							</div>
							<label class="col-md-4 control-label">Nume Mama</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="mama"
									value="{{ $patient->mama }}">
							</div>
							<label class="col-md-4 control-label">Nume Tata</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="tata"
									value="{{ $patient->tata }}">
							</div>
							<label class="col-md-4 control-label">Adresa</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="adresa"
									ng-init="adresa='{{ $patient->adresa }}'" ng-model="adresa" required ng-pattern="/^[a-zA-Z\s0-9\/]*$/">
								<span style="color: red" ng-show="(create_form.adresa.$dirty || submitted) && create_form.adresa.$invalid"> 
									<span ng-show="create_form.adresa.$error.required">Câmpul este obligatoriu.</span>
									<span ng-show="create_form.adresa.$error.pattern">Câmpul poate contine doar litere si spatiu.</span>
								</span>
							</div>
							
							<label class="col-md-4 control-label">Judet</label>
							<div class="col-md-6">
								
								<select class="form-control" id="judet" name="judet"  ng-model="judet" ng-init="judet='{{ $patient->judet_id }}'" ng-change="change_oras(judet)" required>
								<option value="">Selecteaza judet</option>
								@foreach ($counties as $county)
								    <option value="{{ $county->id }}">{{ $county->name }}</option>
								@endforeach
								</select>
								<span style="color: red" ng-show="(create_form.judet.$dirty || submitted) && create_form.judet.$invalid"> 
									<span ng-show="create_form.judet.$error.required">Câmpul Judet este obligatoriu.</span>
								</span>
							</div>
							<label class="col-md-4 control-label">Localitate</label>
							<div class="col-md-6">
								<select class="form-control" id="localitate" name="localitate"  ng-init="localitate='{{ $patient->localitate_id }}'" initial_value='{{ $patient->localitate_id }}' ng-model="localitate" required>
								<option value="">Selecteaza judet intai</option>
								<option ng-selected="@{{town.id == localitate}}" ng:repeat="town in choose_town" value="@{{town.id}}">     
                                    @{{town.name}}
                                </option>
								</select>
								<span style="color: red" ng-show="(create_form.localitate.$dirty || submitted) && create_form.localitate.$invalid"> 
									<span ng-show="create_form.localitate.$error.required">Câmpul Localitate este obligatoriu.</span>
								</span>
							</div>
							<label class="col-md-4 control-label">Email</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="email"
									ng-init="email='{{ $patient->email }}'" ng-model="email" required ng-pattern="/^[a-z]+[a-z0-9._]+@[a-z0-9]+\.[a-z0-9.]{2,5}$/"> 
								<span style="color: red" ng-show="(create_form.email.$dirty || submitted) && create_form.email.$invalid"> 
									<span ng-show="create_form.email.$error.required">Câmpul Email este obligatoriu.</span>
									<span ng-show="create_form.email.$error.pattern">Câmpul Email .</span>
								</span>
							</div>
							<label class="col-md-4 control-label">Telefon</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="telefon"
									ng-init="telefon='{{ $patient->telefon }}'" ng-model="telefon" required> 
								<span style="color: red" ng-show="(create_form.telefon.$dirty || submitted) && create_form.telefon.$invalid"> 
									<span ng-show="create_form.telefon.$error.required">Câmpul Telefon este obligatoriu.</span>
								</span>
							</div>
							<label class="col-md-4 control-label">Observatii Medicale</label>
							<div class="col-md-6">
								<textarea id="observatii_medicale" class="form-control" name="observatii_medicale">{{ $patient->observatii_medicale }}</textarea>
							</div>
							<label class="col-md-4 control-label"> </label>
							<div class="col-md-6">
								<input class="btn btn-primary btn-block"  type="button" ng-click="submit()" value="Salveaza" >
							</div>
						</div>
						<input type="hidden" name="first_step" id="first_step" ng-model="first_step" value="1">
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
