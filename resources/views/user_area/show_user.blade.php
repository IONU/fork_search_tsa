@extends('layouts.app') @section('content')
    <script data-require="angular-messages@*" data-semver="1.4.3" src="https://code.angularjs.org/1.4.3/angular-messages.min.js"></script>
    <script type="text/javascript" src="/js/ngRemoteValidate.0.6.1.min.js"></script>
    <script src="/js/user_area/create_pacient.js"></script>
    <style>
        ul{
            list-style: none outside none;
        }

        .detalii span{
            font-weight: bold;
        }
    </style>

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Detalii Pacient</div>

                    <div class="panel-body">
                        <ul class="detalii">
                            <li>
                                <span class="col-md-6 control-span ">Nume</span>
                                <div class="col-md-6">
                                    {{ $user->name }}
                                </div>
                            </li>
                            <li>
                                <span class="col-md-6 control-span">Adresa Email</span>
                                <div class="col-md-6">
                                    <div>{{ $user->email }}</div>
                                </div>
                            </li>
                            <li>
                                <span class="col-md-6 control-span">Parola</span>
                                <div class="col-md-6">
                                    <div>******</div>
                                </div>
                            </li>
                            <li>
                                <span class="col-md-6 control-span">Tip utilizator</span>
                                <div class="col-md-6">
                                    <div>{{ $user->user_type }}</div>
                                </div>
                            </li>

                        </ul>

                    </div>

                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <ul>
                            <li>
                                <div class="btn-group" role="group" aria-label="...">
                                    <a href='/user/edit'><button type="button" class="btn  btn-info">Editeaza Utilizator</button></a>
                                    <a href='/user/changepassword'><button type="button" class="btn btn-warning">Schimba Parola</button></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
