@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Editează datele utilizatorului</div>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> Rezolvati erorile.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal" role="form" method="POST" action="/user/save">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
                                <label class="col-md-4 control-label">Nume</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" value="{{$user->name}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Adresa Email</label>
                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="email" value="{{ $user->email}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Parola</label>
                                <div class="col-md-6">
                                    <input type="password" readonly class="form-control" name="password" value="******">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Tip utilizator</label>
                                <div class="col-md-6">
                                    <select name="user_type" class="form-control">
                                        <option value="">Selectați o variantă</option>
                                        <option value="medic"  {{$user->user_type == 'medic'? "selected":""}}   >Medic</option>
                                        <option value="psiholog" {{$user->user_type == 'psiholog'? "selected":""}}  >Psiholog</option>
                                        <option value="parinte" {{$user->user_type == 'parinte'? "selected":""}} >Părinte</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                       Salvează
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
