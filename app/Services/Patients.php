<?php
namespace App\Services;

use \App\Models\Patient;
use \App\Dao\BasicDao;
use Illuminate\Support\Facades\DB;

class Patients extends BasicDao
{
	static $model = '\App\Models\Patient';
	
	public function get_user_patients($user){
		return $user->patients()->get();
	}
	
	static function get_user_pacient($user_id, $pacient_id){
		return Patient::
		join('counties', 'patients.judet', '=', 'counties.id')
		->join('towns', 'patients.localitate', '=', 'towns.id')
		->select('patients.*' , 'counties.name AS judet' , 'towns.name AS localitate' , 'counties.id AS judet_id' , 'towns.id AS localitate_id')
		    
		->where('user_id', $user_id)
		->where('patients.id', $pacient_id)
		
		->firstOrFail();
	}

	static public function get_users_pacients(){
		$query = Patient::join('users', 'users.id', '=', 'user_id')->get();
		return $query;
	}

	public function pacients_number($start_date, $end_date, $user_id = null)
	{
		$query = Patient::select(DB::raw('count(patients.id) as items'), 'user_id', 'posibil_autism', 'name')
			->join('users', 'users.id', '=', 'user_id')
			->whereBetween('tested_at', array($start_date, $end_date))
			->groupBy('users.id')
			->groupBy('posibil_autism');
		if ($user_id != null) {
			$query = $query->where('user_id', '=', $user_id);
		}
		$query = $query->get();
		return $query;
	}
	static function get_user_pacient_search($user_id, $search, $start, $per_page){
		return Patient::
		select('nume', 'prenume','cnp', 'email', 'posibil_autism', 'tested_at')
		->where('user_id', $user_id);
//		->where(function ($query) use ($search) {
//			$query->where('patients.nume', 'like', "%$search%");
//			$query->orWhere('patients.prenume', 'like', "%$search%");
//			$query->orWhere('patients.cnp', 'like', "%$search%");
//			$query->orWhere('patients.email', 'like', "%$search%");
//			$query->orWhere('patients.tested_at', 'like', "%$search%");
//		});
// 		->skip($start)->take($per_page)
		//->get();
	}
}