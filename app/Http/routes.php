<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'FrontEndController@index');

Route::get('/vizualizeaza_chestionar', 'Front_Area\IntrebariController@view_inrebari');
Route::get('/questions', 'Front_Area\IntrebariController@show_questions');
Route::post('/proceseaza_chestionar', 'Front_Area\IntrebariController@process_questions');

Route::get('home', 'BackEndController@index');

Route::get('/pacient/adauga', 'User_Area\PacientController@create');
Route::post('/pacient/verfy_cnp/', 'User_Area\PacientController@verfy_cnp');

// Store a new pacient...
Route::post('/pacient/do_edit/{patient_id}', 'User_Area\PacientController@do_edit');
// Edit a pacient...
Route::post('/pacient/store', 'User_Area\PacientController@store');
// get towns with county...
Route::get('/pacient/get_towns/{county_id}', 'User_Area\PacientController@get_towns');
// get patients
Route::get('/pacient/list', 'User_Area\PacientController@list_patients');
// get patients
Route::get('/pacient/datatables', 'User_Area\PacientController@datatables');
// edit patients
Route::get('/pacient/edit/{pacient_id}', 'User_Area\PacientController@edit');
// detalii patients
Route::get('/pacient/detalii/{pacient_id}', 'User_Area\PacientController@detalii');

Route::get('/pacient/chestionar/{pacient_id}', 'User_Area\QuestionnaireController@show_questionnaire');
Route::get('/pacient/chestionar/{pacient_id}', 'User_Area\QuestionnaireController@show_questionnaire');

Route::post('/pacient/submit_questionnaire', 'User_Area\QuestionnaireController@submit_questionnaire');
Route::get('/pacient/vizualizare_chestionar', 'User_Area\QuestionnaireController@view_questionnaire');

Route::get('/pacient/rapoarte/statistici_pacienti', 'User_Area\ReportsController@patients_statistics');



Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
    'user' => 'User_Area\UserController',
]);
Route::get('/questions_list', 'Admin_Area\AdminController@show_questions');
Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware'], function()
{
    Route::get('/admin','Admin_Area\AdminController@index');
    Route::resource('/do_add_questions', 'Admin_Area\AdminController@do_add_questions');
    Route::post('/delete_question/{question_id}', 'Admin_Area\AdminController@delete_question');
    Route::get('/admin/lista_pacienti', 'Admin_Area\PatientController@list_patients');
    Route::get('/admin/lista_utilizatori', 'Admin_Area\UserController@list_users');

});

