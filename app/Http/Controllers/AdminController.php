<?php namespace App\Http\Controllers;

use App\Models\Question;
use App\Models\QuestionAnswer;
use App\Services\QuestionAnswerService;
use App\Services\QuestionService;

class AdminController extends BackEndController {

    public function __construct(){
        parent::__construct();

    }

    function index(){
        return view ('admin.questions.view_questions');
    }

    public function show_questions(){
        $question_answer = new QuestionAnswerService();
        $questions = Question::orderBy('ordine','asc')->get();
        foreach($questions as $question) {
            $question->answers = $question_answer->get_answers_for_question($question->id);
        }
        return json_encode($questions);
    }

    public function do_add_questions()
    {
        $data = \Input::all();
        $answers =[];
        if(isset($data['answers']) && !empty($data['answers'])){
            $answers = $data['answers'];
            unset($data['answers']);
        }

        $questions = new QuestionService();
        $question_id = $questions->save_question($data);
        $question_answer = new QuestionAnswerService();
        if(!empty($answers)){
            foreach($answers as $answer){
                $answer['question_id'] = $question_id;
                $question_answer->save_question_answer($answer);
            }
        }

    }


    public function delete_question($question_id)
    {
        $question = Question::find($question_id);
        $question_answer = new QuestionAnswerService();
        $answers = $question_answer->get_answers_for_question($question_id);
        if(!empty($answers)){
            foreach($answers as $answer){
                $question_answer_obj = QuestionAnswer::find($answer->id);
                $question_answer_obj->delete();
            }
        }
        $question->delete();
//        return Redirect::to('show_projects');
    }

}