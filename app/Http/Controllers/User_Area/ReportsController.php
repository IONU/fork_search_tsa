<?php
/**
 * Created by PhpStorm.
 * User: IONU
 * Date: 08-11-2015
 * Time: 13:01
 */

namespace App\Http\Controllers\User_Area;


use App\Http\Controllers\BackEndController;
use App\Services\Patients;
use Illuminate\Support\Facades\Auth;

class ReportsController  extends BackEndController
{

    public function __construct()
    {
        parent::__construct();
        if (Auth::user()->user_type != 'medic') {
            abort(403);
        }
    }

    public function patients_statistics(){

        $patients = new Patients();
        $patients_number = $patients->pacients_number('2015-10-01', '2015-11-30', Auth::user()->id);
        return view('reports.statistic_list')->with('pacients_numbers', $patients_number);
    }

}