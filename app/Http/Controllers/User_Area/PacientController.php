<?php 
namespace App\Http\Controllers\User_Area;

use App\Http\Controllers\BackEndController;
use Illuminate\Http\Request;
use App\Services;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\Patient;
use App\Services\Counties;
use App\Services\Towns;
use App\Services\Users;
use App\Services\Patients;
use yajra\Datatables\Datatables;

class PacientController extends BackEndController {

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('home');
	}

	public function create() {
		$counties = Counties::get_rows_order_by([], 'name');
		
		$data['counties'] = $counties;
		
		return view('user_area/create_pacient', $data);
	}
	public function edit(Request $request) {
		$counties = Counties::get_rows_order_by([], 'name');
		
		$data['counties'] = $counties;
		
		$patient = Patients::get_user_pacient($request->user()->id, $request->pacient_id);
		$data['patient'] = $patient;
		
		return view('user_area/edit_pacient', $data);
	}

	public function detalii(Request $request) {
		$counties = Counties::get_rows_order_by([], 'name');
		
		$data['counties'] = $counties;
		try {
			$patient = Patients::get_user_pacient($request->user()->id, $request->pacient_id);
		} catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
			abort(403);
			exit;
		}

		$data['patient'] = $patient;

		return view('user_area/detalii_pacient', $data);
	}

	public function verfy_cnp(Request $request) {
		try{
			Patient::where('cnp',$request->value)->firstOrFail();
			echo json_encode(['isValid'=>false, 'value'=>$request->value]);
		}catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e){
			
			echo json_encode(['isValid'=>true, 'value'=>$request->value]);

		}
	}
	
	public function get_towns(Request $request) {
		$towns = Towns::get_rows_order_by(['county_id'=>$request->county_id], "name");
		
		echo json_encode($towns);
	}


	/**
	 * Store a new pacient.
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$patient = new Patient;
		$user = $request->user();
		
		$users_service = new Users();
		$users_service->save_pacient($user, $patient, $request);
		return redirect("/pacient/detalii/".$patient->id);
	}


	/**
	 * Edit pacient.
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function do_edit(Request $request)
	{
		$this->validate($request, [
	        'nume' => 'required|string',
	        'prenume' => 'required|string',
	        'cnp' => 'required|numeric',
	        'mama' => 'string',
	        'tata' => 'string',
	        'adresa' => 'required',
	        'localitate' => 'required',
	        'judet' => 'required',
	        'email' => 'required|email',
	        'telefon' => 'required',
	    ]);

		try {
			$patient = Patients::get_user_pacient($request->user()->id, $request->patient_id);
		}catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e){
			die("nu exista pacient");
		}

		$patient['nume'] = $request->nume;
		$patient['prenume'] = $request->prenume;
		$patient['cnp'] = $request->cnp;
		$patient['mama'] = $request->mama;
		$patient['tata'] = $request->tata;
		$patient['adresa'] = $request->adresa;
		$patient['judet'] = $request->judet;
		$patient['localitate'] = $request->localitate;
		$patient['email'] = $request->email;
		$patient['telefon'] = $request->telefon;
		$patient['observatii_medicale'] = $request->observatii_medicale;

		$user = $request->user();
		$users_service = new Users();
		$users_service->save_pacient($user, $patient, $patient->id);
		
		return redirect("/pacient/detalii/".$request->patient_id);
		
	}
	
	public function list_patients(Request $request){
// 		$users_service = new Users();
		
// 		$patients = $users_service->get_user_patients($request->user());

// 		$data['patients'] = $patients;
		
		return view('user_area/list_pacients', []);
	}

	public function datatables(Request $request){
		$users_service = new Users();
		//search[value]
		$patients = Patients::get_user_pacient_search($request->user()->id, $request->input('search.value'), $request->input('start'), $request->input('length'));


		return  Datatables::of($patients)->make(true);
		DB::enableQueryLog();
		print_r(DB::getQueryLog());
	}
}
