<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionPatient extends Model{

    protected $table = 'questions_to_pacients';

    public $timestamps = false;
}